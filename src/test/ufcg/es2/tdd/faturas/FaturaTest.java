package ufcg.es2.tdd.faturas;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.*;

public class FaturaTest {

    @Test
    public void testaValorInvalido() {
        int[] valoresInvalidos = new int[]{-1, -2, -2000, -999999};

        for (int valor : valoresInvalidos) {
            try {
                (new Fatura())
                        .setValor(valor);
                fail("Deveria falhar quando valor tem valor inválido igual a " +
                        valor + ".");
            } catch (IllegalArgumentException e) {
                assertEquals("Deveria apresentar mensagem específica.",
                        "Fatura não pode ter valor nulo ou negativo.", e.getMessage());
            }
        }
    }

    @Test
    public void testaValorValido() {
        int[] valoresValidos = new int[]{0, 1, 2, 3000, 1000000, 999999999};

        for (int valor : valoresValidos) {
            Fatura fatura = (new Fatura())
                    .setValor(valor);
            assertTrue("Valor incorreto, deveria ser igual a " + valor + ".",
                    fatura.getValor() == valor);
        }
    }

    @Test
    public void testaDataInvalida() {
        try {
            (new Fatura())
              .setData(null);
            fail("Deveria falhar com data nula.");
        } catch (NullPointerException e) {
            assertEquals("Deveria ter uma mensagem de erro específica.",
              "Fatura não pode ter data nula.", e.getMessage());
        }
    }

    @Test
    public void testaDataValida() {
        LocalDate [] datasValidas = new LocalDate[]{
            LocalDate.of(1500, Month.FEBRUARY, 22),
            LocalDate.of(1955, Month.JUNE, 15),
            LocalDate.of(2000, Month.MARCH, 22),
            LocalDate.of(3000, Month.DECEMBER, 31)
        };

        for (LocalDate data : datasValidas) {
            Fatura fatura = (new Fatura())
                    .setData(data);

            assertTrue("Valor do ano incorreto, deveria ser " + data.getYear() + ".",
                    fatura.getData().getYear() == data.getYear());
            assertTrue("Valor do mês incorreto, deveria ser " + data.getMonth() + ".",
                    fatura.getData().getMonth().getValue() == data.getMonth().getValue());
            assertTrue("Valor do dia incorreto, deveria ser " + data.getDayOfMonth() + ".",
                    fatura.getData().getDayOfMonth() == data.getDayOfMonth());
        }
    }

    @Test
    public void testaClienteInvalido() {
        try {
            (new Fatura())
                    .setCliente(null);
            fail("Deveria falhar com cliente nulo.");
        } catch (NullPointerException e) {
            assertEquals("Deveria ter uma mensagem de erro específica.",
                    "Cliente não pode ser nulo.", e.getMessage());
        }
    }

    @Test
    public void testaClienteValido() {
        Cliente [] clientesValidos = {
                (new Cliente()).setNome("Durval Pinheiro").setEstado("PB"),
                (new Cliente()).setNome("Joao Welligton").setEstado("AM"),
                (new Cliente()).setNome("Eduardo Costa").setEstado("SP")
        };

        for (Cliente cliente : clientesValidos) {
            Fatura fatura = (new Fatura()).setCliente(cliente);

            assertEquals("Valor do nome incorreto, deveria ser: " + cliente.getNome(),
                    cliente.getNome(), fatura.getCliente().getNome());
            assertEquals("Valor do estado incorreto, deveria ser: " + cliente.getEstado(),
                    cliente.getEstado(), fatura.getCliente().getEstado());
        }
    }

    @Test
    public void testaCodigoInvalido() {
        String [] codigosInvalidos = new String[]{"-1", "-2212343", " 2000", "999999 ", "123", "123456789", "232323AB", "a", "1s", ""};

        try {
            (new Fatura())
                    .setCodigo(null);
            fail("Deveria falhar quando código é nulo.");
        } catch (NullPointerException e) {
            assertEquals("Deveria apresentar mensagem específica.",
                    "Fatura não pode ter código nulo.", e.getMessage());
        }

        for (String codigo : codigosInvalidos) {
            try {
                (new Fatura())
                        .setCodigo(codigo);
                fail("Deveria falhar quando código é inválido igual a " + codigo + ".");
            } catch (IllegalArgumentException e) {
                assertEquals("Deveria apresentar mensagem específica.",
                        "Fatura não pode ter código inválido.", e.getMessage());
            }
        }
    }

    @Test
    public void testaCodigoValido() {
        String[] codigoValidos = new String[]{"12345678", "44444444", "09090909"};

        for (String codigo : codigoValidos) {
            Fatura fatura = (new Fatura())
                    .setCodigo(codigo);
            assertTrue("Código incorreto, deveria ser igual a " + codigo + ".",
                    fatura.getCodigo() == codigo);
        }
    }
}
