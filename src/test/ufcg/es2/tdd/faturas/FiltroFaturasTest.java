package ufcg.es2.tdd.faturas;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FiltroFaturasTest {

    private List<Fatura> listaVazia;
    private List<Fatura> listaNull;
    private FiltroFaturas filtro;
    private Fatura fatura1000, fatura2000, fatura9000;
    private Fatura fatura2000MesAtual, fatura2000MesAnterior, fatura2300MesAtual, fatura2300MesAnterior;
    private Fatura fatura2500ClienteRecente, fatura2500ClienteAntigo, fatura2800ClienteRecente, fatura2800ClienteAntigo;
    private Fatura fatura3000ClienteRecente, fatura3000ClienteAntigo, fatura3500ClienteRecente, fatura3500ClienteAntigo;
    private Fatura fatura4000ClienteNaoSulista, fatura4000ClienteSulista, fatura8000ClienteNaoSulista, fatura8000ClienteSulista;

    @Before
    public void preparaListas() {
        Cliente clienteRecente = (new Cliente())
                .setDataDeInclusao(LocalDate.now());
        Cliente clienteAntigo = (new Cliente())
                .setDataDeInclusao(LocalDate.now().minus(Period.ofMonths(3)));

        fatura1000 = (new Fatura())
                .setValor(1000);
        fatura2000 = (new Fatura())
                .setValor(2000);
        fatura9000 = (new Fatura())
                .setValor(9000);

        fatura2000MesAtual = (new Fatura())
                .setValor(2000)
                .setData(LocalDate.now());
        fatura2000MesAnterior = (new Fatura())
                .setValor(2000)
                .setData(LocalDate.now().minus(Period.ofMonths(2)));
        fatura2300MesAtual = (new Fatura())
                .setValor(2300)
                .setData(LocalDate.now());
        fatura2300MesAnterior = (new Fatura())
                .setValor(2300)
                .setData(LocalDate.now().minus(Period.ofMonths(2)));

        fatura2500ClienteRecente = (new Fatura())
                .setValor(2500)
                .setCliente(clienteRecente);
        fatura2500ClienteAntigo = (new Fatura())
                .setValor(2500)
                .setCliente(clienteAntigo);
        fatura2800ClienteRecente = (new Fatura())
                .setValor(2800)
                .setCliente(clienteRecente);
        fatura2800ClienteAntigo = (new Fatura())
                .setValor(2800)
                .setCliente(clienteAntigo);

        fatura3000ClienteRecente = (new Fatura())
                .setValor(3000)
                .setCliente(clienteRecente);
        fatura3000ClienteAntigo = (new Fatura())
                .setValor(3000)
                .setCliente(clienteAntigo);
        fatura3500ClienteRecente = (new Fatura())
                .setValor(3500)
                .setCliente(clienteRecente);
        fatura3500ClienteAntigo = (new Fatura())
                .setValor(3500)
                .setCliente(clienteAntigo);

        Cliente clienteNaoSulista = (new Cliente())
                .setEstado("PB");
        Cliente clienteSulista = (new Cliente())
                .setEstado("RS");

        fatura4000ClienteNaoSulista = (new Fatura())
                .setValor(4000)
                .setCliente(clienteNaoSulista);
        fatura4000ClienteSulista = (new Fatura())
                .setValor(4000)
                .setCliente(clienteSulista);
        fatura8000ClienteNaoSulista = (new Fatura())
                .setValor(8000)
                .setCliente(clienteNaoSulista);
        fatura8000ClienteSulista = (new Fatura())
                .setValor(8000)
                .setCliente(clienteSulista);

        listaVazia = new ArrayList<>();
        listaNull = null;
        filtro = new FiltroFaturas();
    }

    @Test
    public void testaListaVazia() {
        List<Fatura> list = filtro.filtra(listaVazia);

        assertTrue("Filtro deveria devolver uma lista vazia.", list.isEmpty());
    }

    @Test
    public void testaListaNull() {
        try {
            filtro.filtra(listaNull);
            fail("Filtro deveria falhar com lista nula.");
        } catch (NullPointerException e) {
            assertEquals("Filtro deveria ter uma mensagem específica.",
                    "Filtro não pode tratar listas de valor nulo.", e.getMessage());
        }
    }

    @Test
    public void testaFiltrarFaturasComValorMenorQue2000() {
        List<Fatura> lista = Stream
                .of(fatura1000)
                .collect(Collectors.toList());

        List<Fatura> listaFiltrada = filtro.filtra(lista);

        assertTrue("Após o filtro, a lista deveria retornar uma lista vazia.",
                listaFiltrada.isEmpty());

        List<Fatura> valoresValidos = Stream
                .of(fatura2000, fatura9000)
                .collect(Collectors.toList());
        List<Fatura> valoresInvalidos = Stream
                .of(fatura1000)
                .collect(Collectors.toList());

        testaFiltroComValoresFalhos(valoresValidos, valoresInvalidos);
    }

    @Test
    public void testaFiltrarFaturasMuitoRecentes() {
        List<Fatura> lista = Stream
                .of(fatura2000MesAnterior)
                .collect(Collectors.toList());

        List<Fatura> listaFiltrada = filtro.filtra(lista);

        assertTrue("Após o filtro, a lista deveria retornar uma lista vazia.",
                listaFiltrada.isEmpty());

        List<Fatura> valoresValidos = Stream
                .of(fatura2000, fatura2000MesAtual, fatura2300MesAtual, fatura9000)
                .collect(Collectors.toList());
        List<Fatura> valoresInvalidos = Stream
                .of(fatura2000MesAnterior, fatura2300MesAnterior)
                .collect(Collectors.toList());

        testaFiltroComValoresFalhos(valoresValidos, valoresInvalidos);
    }

    @Test
    public void testaFiltrarFaturasComClienteInclusoRecentemente() {
        List<Fatura> lista = Stream
                .of(fatura2000MesAnterior)
                .collect(Collectors.toList());

        List<Fatura> listaFiltrada = filtro.filtra(lista);

        assertTrue("Após o filtro, a lista deveria retornar uma lista vazia.",
                listaFiltrada.isEmpty());

        List<Fatura> valoresValidos = Stream
                .of(fatura2000, fatura2000MesAtual, fatura2300MesAtual, fatura2500ClienteRecente, fatura2800ClienteRecente,
                    fatura9000, fatura3000ClienteRecente, fatura3000ClienteAntigo, fatura3500ClienteRecente, fatura3500ClienteAntigo)
                .collect(Collectors.toList());
        List<Fatura> valoresInvalidos = Stream
                .of(fatura2500ClienteAntigo, fatura2800ClienteAntigo)
                .collect(Collectors.toList());

        testaFiltroComValoresFalhos(valoresValidos, valoresInvalidos);
    }

    @Test
    public void testaFiltrarFaturasComClienteSulista() {
        List<Fatura> lista = Stream
                .of(fatura2000MesAnterior)
                .collect(Collectors.toList());

        List<Fatura> listaFiltrada = filtro.filtra(lista);

        assertTrue("Após o filtro, a lista deveria retornar uma lista vazia.",
                listaFiltrada.isEmpty());

        List<Fatura> valoresValidos = Stream
                .of(fatura2000, fatura2000MesAtual, fatura2300MesAtual, fatura2500ClienteRecente, fatura2800ClienteRecente,
                        fatura9000, fatura3000ClienteRecente, fatura3000ClienteAntigo, fatura3500ClienteRecente, fatura3500ClienteAntigo,
                        fatura4000ClienteNaoSulista, fatura8000ClienteNaoSulista)
                .collect(Collectors.toList());
        List<Fatura> valoresInvalidos = Stream
                .of(fatura4000ClienteSulista, fatura8000ClienteSulista)
                .collect(Collectors.toList());

        testaFiltroComValoresFalhos(valoresValidos, valoresInvalidos);
    }

    private void testaFiltroComValorFalho(List<Fatura> valoresValidos, Fatura valorFalho){
        Random rand = new Random();
        int NUMERO_DE_REARRANJOS = 5;
        for (int i = 0; i < NUMERO_DE_REARRANJOS; i++) {
            Collections.shuffle(valoresValidos);
            int index = rand.nextInt(valoresValidos.size());

            List<Fatura> listaAFiltrar = new ArrayList<>(valoresValidos);
            listaAFiltrar.add(index, valorFalho);

            List<Fatura> valoresFiltrados = filtro.filtra(listaAFiltrar);

            assertFalse("Após o filtro, a lista não deveria retornar uma lista vazia.",
                    valoresFiltrados.isEmpty());

            for (int j = 0; j < valoresValidos.size(); j++) {
                comparaOrdemDasFaturas(valoresValidos.get(j), valoresFiltrados.get(j));
            }
        }
    }

    private void testaFiltroComValoresFalhos(List<Fatura> valoresValidos, List<Fatura> valoresFalhos) {
        for (Fatura valorFalho : valoresFalhos) {
            testaFiltroComValorFalho(valoresValidos, valorFalho);
        }
    }

    private void comparaOrdemDasFaturas(Fatura esperado, Fatura recebido) {
        String mensagem = "Após o filtro, a ordem dos dados deve ser mantida.\n" +
                "\tFatura Esperada: " + esperado.toString() + "\n" +
                "\tFatura Recebida: " + recebido.toString();

        assertEquals(mensagem, esperado, recebido);
    }
}
