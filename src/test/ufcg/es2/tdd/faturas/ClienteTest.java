package ufcg.es2.tdd.faturas;

import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ClienteTest {

    @Test
    public void testaDataDeInclusaoInvalida(){
        try {
            (new Cliente())
                    .setDataDeInclusao(null);
            fail("Deveria falhar com data nula.");
        } catch (NullPointerException e) {
            assertEquals("Deveria ter uma mensagem de erro específica.",
                    "Cliente não pode ter data de inclusão nula.", e.getMessage());
        }
    }

    @Test
    public void testaDataDeInclusaoValida() {
        testaDataDeInclusaoValida(22, Month.FEBRUARY, 1500);
        testaDataDeInclusaoValida(15, Month.JUNE, 1995);
        testaDataDeInclusaoValida(22, Month.MARCH, 2000);
        testaDataDeInclusaoValida(31, Month.DECEMBER, 3000);
    }

    private void testaDataDeInclusaoValida(int dia, Month mes, int ano) {
        Cliente cliente = (new Cliente())
                .setDataDeInclusao(LocalDate.of(ano, mes, dia));

        assertTrue("Valor do ano incorreto, deveria ser " + ano + ".",
                cliente.getDataDeInclusao().getYear() == ano);
        assertTrue("Valor do mês incorreto, deveria ser " + mes + ".",
                cliente.getDataDeInclusao().getMonth().getValue() == mes.getValue());
        assertTrue("Valor do dia incorreto, deveria ser " + dia + ".",
                cliente.getDataDeInclusao().getDayOfMonth() == dia);
    }

    @Test
    public void testaEstadoInvalido() {
        try {
            (new Cliente())
                    .setEstado(null);
            fail("Deveria falhar com Estado nulo.");
        } catch (NullPointerException e) {
            assertEquals("Deveria ter uma mensagem de erro específica.",
                    "Cliente não pode ter estado nulo.", e.getMessage());
        }

        String [] estadosInvalidos = new String[] {
            "NO", "Paraíba", "", "."
        };


        for (String estado : estadosInvalidos) {
            try {
                (new Cliente())
                        .setEstado(estado);
                fail("Deveria falhar com Estado inválido = " + estado + ".");
            } catch (IllegalArgumentException e) {
                assertEquals("Deveria ter uma mensagem de erro específica.",
                        "Grafia de Estado inválida, insira apenas estados do Brasil da forma: PB, AL, etc.", e.getMessage());
            }
        }
    }

    @Test
    public void testaEstadoValido(){
        String [] estadosValidos = new String[] {
            "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO",
            "MA", "MG", "MS", "MT", "PA", "PB", "PE", "PI", "PR",
            "RJ", "RN", "RO", "RR", "RS", "SC", "SE", "SP", "TO"
        };

        for (String estado : estadosValidos) {
            Cliente cliente = (new Cliente())
                    .setEstado(estado);

            assertEquals("Deveria mostrar o argumento correto.", estado, cliente.getEstado());
        }
    }

    @Test
    public void testaNomeInvalido() {
        try {
            (new Cliente())
                    .setNome(null);
            fail("Deveria falhar com Nome nulo.");
        } catch (NullPointerException e) {
            assertEquals("Deveria ter uma mensagem de erro específica.",
                    "Cliente não pode ter nome nulo.", e.getMessage());
        }

        String [] nomesInvalidos = new String[] {
                "123456", "1", "Dennis15", "dds_prog", "Mc-Donalds", "abcdefghijklm nopqrstuvwxyz "
        };


        for (String nome : nomesInvalidos) {
            try {
                (new Cliente())
                        .setNome(nome);
                fail("Deveria falhar com Nome inválido = " + nome + ".");
            } catch (IllegalArgumentException e) {
                assertEquals("Deveria ter uma mensagem de erro específica.",
                        "Grafia de Nome inválida, insira apenas caracteres do alfabeto + espaço.", e.getMessage());
            }
        }
    }

    @Test
    public void testaNomeValido(){
        String [] nomesValidos = new String[] {
                "Dennis", "Olivia Palito", "Cristiano Ronaldo", "abcdefghijklm nopqrstuvwxyz"
        };

        for (String nome : nomesValidos) {
            Cliente cliente = (new Cliente())
                    .setNome(nome);

            assertEquals("Deveria mostrar o argumento correto.", nome, cliente.getNome());
        }
    }
}
