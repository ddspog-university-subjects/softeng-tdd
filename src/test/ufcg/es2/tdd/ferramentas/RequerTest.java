package ufcg.es2.tdd.ferramentas;

import org.junit.Test;

import static org.junit.Assert.*;

public class RequerTest {
    @Test
    public void testaRequerArgumentoNaoNegativo(){
        String mensagem = "Valor deveria ser positivo";

        int[] valoresInvalidos = new int[]{-1, -2, -1000000};

        for(int valor : valoresInvalidos) {
            testaRequerArgumentoNaoNegativoInvalido(mensagem, valor);
        }

        int[] valoresValidos = new int[]{0, 1, 2, 1000000};

        for(int valor : valoresValidos) {
            testaRequerArgumentoNaoNegativoValido(mensagem, valor);
        }
    }

    private void testaRequerArgumentoNaoNegativoValido(String mensagem, int valor) {
        Requer.argumentoNaoNegativo(valor, mensagem);
    }

    private void testaRequerArgumentoNaoNegativoInvalido(String mensagem, int valor) {
        try {
            Requer.argumentoNaoNegativo(valor, mensagem);
            fail("Deveria lançar uma exceçao aqui.");
        } catch (IllegalArgumentException e){
            assertEquals("Mensagem de erro deveria ser igual.", mensagem, e.getMessage());
        }
    }

    @Test
    public void testaRequerArgumentoNaoNulo() {
        String mensagem = "Valor deveria ser não-nulo.";

        try {
            Requer.argumentoNaoNulo(null, mensagem);
            fail("Deveria lançar uma exceçao aqui.");
        } catch (NullPointerException e) {
            assertEquals("Mensagem de erro deveria ser igual.", mensagem, e.getMessage());
        }

        Requer.argumentoNaoNulo("", mensagem);
    }

    @Test
    public void testaRequerArgumentoSeguindoPadrao() {
        String mensagem = "Você deveria seguir o padrão.";
        String padrao = "^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$";

        String[] valoresInvalidos = new String[]{"#GGGGGG", "#AS231", "", ".", "Amem"};

        for(String valor : valoresInvalidos) {
            testaRequerArgumentoSeguindoPadraoInvalido(mensagem, valor, padrao);
        }

        String[] valoresValidos = new String[]{"#FFF", "#FFFFFF", "#000000", "#BDBDBD"};

        for(String valor : valoresValidos) {
            testaRequerArgumentoSeguindoPadraoValido(mensagem, valor, padrao);
        }
    }

    private void testaRequerArgumentoSeguindoPadraoInvalido(String mensagem, String valor, String padrao) {
        try {
            Requer.argumentoSeguindoPadrao(valor, padrao, mensagem);
            fail("Deveria lançar uma exceçao aqui.");
        } catch (IllegalArgumentException e){
            assertEquals("Mensagem de erro deveria ser igual.", mensagem, e.getMessage());
        }
    }

    private void testaRequerArgumentoSeguindoPadraoValido(String mensagem, String valor, String padrao) {
        Requer.argumentoSeguindoPadrao(valor, padrao, mensagem);
    }
}