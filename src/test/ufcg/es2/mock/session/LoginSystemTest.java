package ufcg.es2.mock.session;

import org.jmock.Expectations;
import org.jmock.integration.junit4.JUnitRuleMockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Suíte de testes para testar a execução de um sistema de login, tendo algumas dependências como HttpSession e
 * Authenticator, que sessão simuladas com JMock.
 */
public class LoginSystemTest {

    @Rule
    public JUnitRuleMockery ctx = new JUnitRuleMockery();

    private LoginSystem system;
    private HttpSession session;
    private Authenticator auth;

    @Before
    public void createContext(){
        session = ctx.mock(HttpSession.class, "session");
        auth = ctx.mock(Authenticator.class, "auth");

        system = new LoginSystem(session, auth);
    }

    @After
    public void resetContext(){
        ctx.assertIsSatisfied();
    }

    @Test
    public void testLoginValidUser(){
        final User john = ctx.mock(User.class);
        String typedPassword = "myDogsBirthday";

        ctx.checking(new Expectations() {{
            oneOf(auth).isValid(john, typedPassword);
            will(returnValue(true));
            oneOf(session).setUser(john);
        }});

        system.login(john, typedPassword);

        assertTrue(system.hasLoggedSomeone());
        assertTrue(system.loggedUser().equals(john));
    }

    @Test
    public void testLoginInvalidUser(){
        final User john = ctx.mock(User.class);
        String typedPassword = "12345";

        ctx.checking(new Expectations() {{
            oneOf(auth).isValid(john, typedPassword);
            will(returnValue(false));
        }});

        system.login(john, typedPassword);

        assertFalse(system.hasLoggedSomeone());
    }

}
