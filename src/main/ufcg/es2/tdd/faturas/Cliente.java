package ufcg.es2.tdd.faturas;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ufcg.es2.tdd.ferramentas.Requer;

import java.time.LocalDate;

/**
 * Classe representando um cliente de um banco.
 */
class Cliente {
    private LocalDate dataDeInclusao;
    private String estado;
    private String nome;

    Cliente() {
        this.dataDeInclusao = LocalDate.now();
    }

    /**
     * Seta uma data de inclusão associada ao Cliente.
     * @param dataDeInclusao data de inclusão a ser associada ao Cliente.
     * @return o Cliente.
     */
    Cliente setDataDeInclusao(LocalDate dataDeInclusao) {
        Requer.argumentoNaoNulo(dataDeInclusao,
                "Cliente não pode ter data de inclusão nula.");

        this.dataDeInclusao = dataDeInclusao;
        return this;
    }

    /**
     * Retorna a data de inclusão associada ao Cliente.
     * @return a data de inclusão associada ao Cliente.
     */
    LocalDate getDataDeInclusao() {
        return dataDeInclusao;
    }

    /**
     * Seta o estado associado ao Cliente.
     * @param estado estado a ser associado ao Cliente.
     * @return o Cliente.
     */
    Cliente setEstado(String estado) {
        Requer.argumentoNaoNulo(estado, "Cliente não pode ter estado nulo.");
        Requer.argumentoSeguindoPadrao(estado, "AC|AL|AM|AP|BA|CE|DF|ES|GO|MA|MG|MS|MT|PA|PB|PE|PI|PR|RJ|RN|RO|RR|RS|SC|SE|SP|TO",
                "Grafia de Estado inválida, insira apenas estados do Brasil da forma: PB, AL, etc.");
        this.estado = estado;
        return this;
    }

    /**
     * Retorna o estado associado ao Cliente.
     * @return o estado associado ao Cliente.
     */
    String getEstado() {
        return estado;
    }

    /**
     * Seta o nome associado ao Cliente.
     * @param nome nome a ser associado ao Cliente.
     * @return o Cliente.
     */
    Cliente setNome(String nome) {
        Requer.argumentoNaoNulo(nome, "Cliente não pode ter nome nulo.");
        Requer.argumentoSeguindoPadrao(nome, "^[a-zA-Z]+(?: [a-zA-Z]+)?$", "Grafia de Nome inválida, insira apenas caracteres do alfabeto + espaço.");

        this.nome = nome;
        return this;
    }

    /**
     * Retorna o estado associado ao Cliente.
     * @return o estado associado ao Cliente.
     */
    String getNome() {
        return nome;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31) // Dois primos randômicos.
                .append(dataDeInclusao)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Cliente)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Cliente cliente = (Cliente) obj;
        return new EqualsBuilder()
                .append(dataDeInclusao, cliente.dataDeInclusao)
                .isEquals();
    }

    @Override
    public String toString() {
        return "Cliente{" + "\n" +
                    "\t\tData de Inclusao = " + dataDeInclusao + "\n" +
                    "\t\tEstado = " + ((estado != null) ? estado : "Nulo") + "\n" +
                "\t}";
    }
}
