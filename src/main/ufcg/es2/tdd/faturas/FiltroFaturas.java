package ufcg.es2.tdd.faturas;

import ufcg.es2.tdd.ferramentas.Requer;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Classe que filtra uma Lista de Faturas removendo faturas
 *   - Se o valor da fatura for menor que 2000;
 *   - Se o valor da fatura	estiver	entre 2000 e 2500 e	a data for menor ou igual a de um mês atrás;
 *   - Se o valor da fatura	estiver	entre 2500 e 3000 e	a data de inclusão do cliente for menor	ou igual a 2 meses atrás;
 *   - Se o valor da fatura for maior que 4000 e pertencer a algum estado da região	Sul	do Brasil.
 */
class FiltroFaturas {

    /* Definindo limites para o filtro */
    private int STAGES[] = new int[]{2000, 2500, 3000, 4000};
    private Period MONTHS_LIMIT[] = new Period[]{Period.ofMonths(1), Period.ofMonths(2)};

    /* Funções para verificar estágio de Valor por fatura */
    private Predicate<Fatura>
            temValorPequenoDemais = f -> f.getValor() < STAGES[0],
            temValorNoPrimeiroGrau = f -> f.getValor() < STAGES[1],
            temValorNoSegundoGrau = f -> f.getValor() < STAGES[2],
            temValorNoTerceiroGrau = f -> f.getValor() >= STAGES[3];

    /* Funções para verificar datas em certos períodos por fatura */
    private Predicate<Fatura>
            faturaAntigaParaPrimeiroGrau = f -> f.getData().compareTo(LocalDate.now().minus(MONTHS_LIMIT[0])) < 0,
            inclusaoAntigaParaSegundoGrau = f -> f.getCliente().getDataDeInclusao().compareTo(LocalDate.now().minus(MONTHS_LIMIT[1])) < 0;

    /* Verificações mais genéricas */
    private Predicate<Fatura> clienteDaFaturaESulista = f ->
            f.getCliente().getEstado().matches("PR|RS|SC");
    private Predicate<Fatura> clienteDaFaturaNaoNulo = f ->
            f.getCliente() != null;

    /**
     * Filtra uma lista de Faturas, retornando algumas delas.
     * @param faturas Lista de Faturas a ser filtrada.
     * @return A lista de faturas já filtrada.
     */
    List<Fatura> filtra(List<Fatura> faturas) {
        Requer.argumentoNaoNulo(faturas,
                "Filtro não pode tratar listas de valor nulo.");

        return faturas.stream().filter(this::mantem).collect(Collectors.toList());
    }

    /**
     * Decide se uma fatura fica ou não fica no método. Essa decisão verifica primeiro se o valor da fatura está em
     * determinado estágio. Para cada estágio, pode haver uma verificação adicional para decidir se a fatura fica ou
     * não na Lista.
     * @param fatura Fatura a ser examinada.
     * @return true, se a Fatura necessita ficar na lista e false, caso contrário.
     */
    @SuppressWarnings("RedundantIfStatement")
    private boolean mantem(Fatura fatura) {
        if(temValorPequenoDemais.test(fatura)) {
            return false;
        } else if (temValorNoPrimeiroGrau.test(fatura) && faturaAntigaParaPrimeiroGrau.test(fatura)) {
            return false;
        } else if (temValorNoSegundoGrau.test(fatura) && clienteDaFaturaNaoNulo.test(fatura) && inclusaoAntigaParaSegundoGrau.test(fatura)) {
            return false;
        } else if (temValorNoTerceiroGrau.test(fatura) && clienteDaFaturaNaoNulo.test(fatura) && clienteDaFaturaESulista.test(fatura)) {
            return false;
        } else {
            return true;
        }
    }
}
