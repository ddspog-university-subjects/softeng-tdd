package ufcg.es2.tdd.faturas;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import ufcg.es2.tdd.ferramentas.Requer;

import java.time.LocalDate;

/**
 * Classe que representa uma fatura de cartão de crédito.
 */
class Fatura {

    private int valor;
    private LocalDate data;
    private Cliente cliente;
    private String codigo;

    Fatura() {
        this.valor = 0;
        this.data = LocalDate.now();
    }

    /**
     * Seta um valor associado a Fatura.
     * @param valor valor associado a Fatura.
     * @return a Fatura.
     */
    Fatura setValor(int valor) {
        Requer.argumentoNaoNegativo(valor,
                "Fatura não pode ter valor nulo ou negativo.");
        this.valor = valor;
        return this;
    }

    /**
     * Retorna o valor associado a Fatura.
     * @return o valor associado a Fatura.
     */
    int getValor() {
        return this.valor;
    }

    /**
     * Seta a data associada a Fatura.
     * @param data data associada a Fatura.
     * @return a Fatura.
     */
    Fatura setData(LocalDate data) {
        Requer.argumentoNaoNulo(data,
                "Fatura não pode ter data nula.");
        this.data = data;

        return this;
    }

    /**
     * Retorna a data associada a Fatura.
     * @return a data associada a Fatura.
     */
    LocalDate getData() {
        return data;
    }

    /**
     * Seta o cliente associado a Fatura.
     * @param cliente cliente associado a Fatura.
     * @return a Fatura.
     */
    Fatura setCliente(Cliente cliente) {
        Requer.argumentoNaoNulo(cliente,
                "Cliente não pode ser nulo.");
        this.cliente = cliente;
        return this;
    }

    /**
     * Retorna o cliente associado a Fatura.
     * @return o cliente associado a Fatura.
     */
    Cliente getCliente() {
        return this.cliente;
    }

    /**
     * Seta o codigo associado a Fatura.
     * @param codigo codigo associado a Fatura.
     * @return a Fatura.
     */
    Fatura setCodigo(String codigo) {
        Requer.argumentoNaoNulo(codigo, "Fatura não pode ter código nulo.");
        Requer.argumentoSeguindoPadrao(codigo, "[0-9]{8}", "Fatura não pode ter código inválido.");

        this.codigo = codigo;
        return this;
    }

    /**
     * Retorna o codigo associado a Fatura.
     * @return o codigo associado a Fatura.
     */
    String getCodigo() {
        return codigo;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31) // Dois primos randômicos.
                .append(valor)
                .append(data)
                .append(cliente)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Fatura)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        Fatura fatura = (Fatura) obj;
        return new EqualsBuilder()
                .append(valor, fatura.valor)
                .append(data, fatura.data)
                .append(cliente, fatura.cliente)
                .isEquals();
    }

    @Override
    public String toString() {
        return "Fatura{" + "\n" +
                "\tValor = " + valor + ",\n" +
                "\tData = " + data + ",\n" +
                "\tCliente = " + ((cliente != null) ? cliente.toString() : "Nulo") + "\n" +
                "}" + "\n";
    }
}
