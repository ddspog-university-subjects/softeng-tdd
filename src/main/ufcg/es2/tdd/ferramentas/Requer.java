package ufcg.es2.tdd.ferramentas;

public class Requer {

    public static void argumentoNaoNegativo(int valor, String mensagem) {
        if(valor < 0) {
            throw new IllegalArgumentException(mensagem);
        }
    }

    public static void argumentoNaoNulo(Object valor, String mensagem) {
        if(valor == null) {
            throw new NullPointerException(mensagem);
        }
    }

    public static void argumentoSeguindoPadrao(String valor, String padrao, String mensagem) {
        if(!valor.matches(padrao)) {
            throw new IllegalArgumentException(mensagem);
        }
    }
}
