package ufcg.es2.mock.session;

/**
 * Class implementing a Login System that can log in users.
 */
public class LoginSystem {

    private HttpSession session;
    private Authenticator auth;
    private User user;
    private boolean hasLoggedSomeone;

    public LoginSystem(HttpSession session, Authenticator auth) {
        this.session = session;
        this.auth = auth;
    }

    public void login(User user, String password) {
        boolean userIsValid = this.auth.isValid(user, password);
        if(userIsValid){
            this.session.setUser(user);
            this.user = user;
            this.hasLoggedSomeone = true;
        } else {
            this.hasLoggedSomeone = false;
        }
    }

    public boolean hasLoggedSomeone() {
        return this.hasLoggedSomeone;
    }


    public User loggedUser() {
        return this.user;
    }
}
