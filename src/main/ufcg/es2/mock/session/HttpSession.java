package ufcg.es2.mock.session;

/**
 * Describes functions needed in HttpSession
 */
interface HttpSession {

    void setUser(User user);
}
