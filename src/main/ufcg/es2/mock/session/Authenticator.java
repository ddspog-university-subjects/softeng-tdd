package ufcg.es2.mock.session;

/**
 * Describes functions needed in Authenticator
 */
interface Authenticator {

    boolean isValid(User user, String typedPassword);
}
